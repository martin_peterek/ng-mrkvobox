import { Injectable } from '@angular/core';
import { ISong } from './main-page.component';

@Injectable({
  providedIn: 'root'
})

export class QueueService {
  private queue: ISong[] = [];

  constructor() { }
  getFirstOfQueue() {
    return this.queue.pop();
  }

  getLenghtofQueue() {
    return this.queue.length;
  }

  addToQueue(song: ISong) {
    this.queue.push(song);
  }
}
