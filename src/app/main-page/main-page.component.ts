import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HostListener } from '@angular/core';
import { QueueService } from './queue.service';

export interface IFiles {
  albumName: string;
  songs: ISong[];
}

export interface ISong {
  albumId: number;
  songName: string;
  songPath: string;
  songImage: string;
  songDuration: number;
}


@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {


  public files: IFiles[];
  public selectedAlbum = 0;
  public selectedSong = 0;
  public audio = new Audio();
  public currentAlbum: any;
  public slider = 0;
  public durationSong = -1;
  public kredit = 0;
  public newSong = true;

  constructor(
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    private _queue: QueueService
  ) { }

  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    switch (event.key) {
      case 'a':
        if (this.selectedAlbum > 0) {
          this.selectedAlbum -= 1;
          this.onSelectA()
        }
        break;
      case 'y':
        if (this.files.length - 1 > this.selectedAlbum) {
          this.selectedAlbum += 1;
          this.onSelectA();
        }
        break;
      case 's':
        if (this.selectedSong > 0) {
          this.selectedSong -= 1;
        }
        break;
      case 'x':
        if (this.files[this.selectedAlbum].songs.length - 1 > this.selectedSong)
          this.selectedSong += 1;

        break;
      case 'Enter':
        this.addSongToPlaying();
        break;
      case ' ':
        // this.playingSong.stop();
        break;
      case '+':
        this.kreditChange(1);
      default:
        break;

    }
  }

  kreditChange(valueOfKredits: number) {
    this.kredit += valueOfKredits;
  }

  addSongToPlaying() {
    if (this.kredit > 0) {
      let queueLenght = this._queue.getLenghtofQueue();
      this._queue.addToQueue((this.files[this.selectedAlbum].songs[this.selectedSong]));

      this.newSong = this.newSong === true ? false : true;




      this.kreditChange(-1);
    }
  }

  onSelectA(): void {
    console.log(this.selectedAlbum)
    if (this.files[this.selectedAlbum].songs[0].songImage == "nopic") {
      this.files[this.selectedAlbum].songs[0].songImage = undefinedAlbumImage;
    }
    this.selectedSong = 0;
  }

  urlSanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  write() {
    console.log(this.selectedSong);
  }

  ngOnInit() {
    this.http.get('/files', { observe: 'response' })
      .subscribe((x: any) => {
        this.files = x.body.data as IFiles[];
        this.onSelectA();
      });

  }

}
