import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';
import { ISong } from '../main-page.component';
import { TimeConvertPipe } from './time-convert.pipe';
import { QueueScheduler } from 'rxjs/internal/scheduler/QueueScheduler';
import { QueueService } from '../queue.service';

@Component({
  selector: 'app-playing-song',
  templateUrl: './playing-song.component.html',
  styleUrls: ['./playing-song.component.css']
})
export class PlayingSongComponent implements OnInit, OnChanges, OnDestroy {
  public slider = 0;
  public durationSong = 0;
  public audio = new Audio();

  private _playSub = new Subscription();

  @Input() set addToQueue(changed: any) {
    if (this._playSub) {
      this._playSub.unsubscribe();
    }
    this.playingSong();
  }

  playingSong() {
    let song = this._queue.getFirstOfQueue();

    if (this.audio.paused === true) {
      if (song) {
        this.durationSong = song.songDuration;
        this.slider = 0;

        this.audio.src = '/files/' + song.songPath;
        this.audio.load();
        this.durationSong = song.songDuration;
        this.audio.play();

        this._playSub = Observable.interval(1000)
          .takeWhile(() => !this.audio.paused)
          .subscribe(i => {
            console.log(this.audio.currentTime);
            this.slider = Math.round(this.audio.currentTime);
            console.log(Math.round(this.audio.currentTime) + ' ' + Math.round(this.durationSong))
            if (Math.round(this.audio.currentTime) >= Math.round(this.durationSong)) {
              this.audio.pause();
              this.playingSong();
            }
          });
        console.log(song);
      }
    } else {
      this._queue.addToQueue(song);
    }

  }
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }

  constructor(private _queue: QueueService) {
  }


  stop() {
    this.audio.pause();
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this._playSub.unsubscribe();
  }

}
