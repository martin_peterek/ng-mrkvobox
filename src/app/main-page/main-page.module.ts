import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainPageComponent } from './main-page.component';
import { PlayingSongComponent } from './playing-song/playing-song.component';
import { TimeConvertPipe } from './playing-song/time-convert.pipe';
import { QueueComponent } from './queue/queue.component';

const routes: Routes = [
  {
    path: '',
    component: MainPageComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule
  ],
  declarations: [MainPageComponent, PlayingSongComponent, TimeConvertPipe, QueueComponent],
  
  exports: [TimeConvertPipe]
})
export class MainPageModule { }
