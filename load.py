import sqlite3
from os import walk
import mutagen.id3
from mutagen import File
import base64


sqlite_file = 'Jukebox.db'
# Connecting to the database file
conn = sqlite3.connect(sqlite_file)
c = conn.cursor()

#Erase all data in database
c.execute('delete from b_album_song')
c.execute('delete from song')
c.execute('delete from album')
c.execute('delete from author')

arrayOfPath = [r"E:\Mrkva Zaloha\Hudba"]

for x in arrayOfPath:
    folders = []
    for (dirpath, dirnames, filenames) in walk(x):
        folders.extend(dirnames)
        break
    for folder in folders:
        songs = []
        for (dirpath, dirnames, filenames) in walk(x+'\\'+folder):
            songs.extend(filenames)
            break

        
        
        try:    
            song = File(x+'\\'+folder+'\\'+songs[0]) #Load first data in album   
            author = song.tags['TPE1'].text[0]
            album = song.tags['TALB'].text[0]
        except:
            author = "Unknow"
            album = folder
            
        try:
            sql = c.execute("select author_id from Author where author_fullname = '"+author+"'").fetchone()[0]
            print (sql)
            c.execute("INSERT INTO album (album_name, author_id)  VALUES ('"+album+"','"+str(int(sql))+"')")
        except:
            c.execute("INSERT INTO author (author_fullname)  VALUES ('"+author+"')")
            
            c.execute("INSERT INTO album (album_name, author_id)  VALUES ('"+album+"',(select author_id from Author where author_fullname = '"+author+"'))")
            

        for y in songs:
            if y[-3:] == 'mp3':
                song = File(x+'\\'+folder+'\\'+y)
                try:    
                    image = base64.b64encode(song.tags['APIC:'].data).decode('utf-8')
                except:
                    image = "nopic"
                c.execute("INSERT INTO song (song_name, song_path, song_image, Song_Duration)  VALUES ('"+y.replace("'", "''")+"','"+x+'\\'+folder+"','"+image+"','"+str(song.info.length)+"')")
                c.execute("INSERT INTO b_album_song (album_id, song_id)  VALUES ( (SELECT MAX(album_id) FROM Album), (SELECT MAX(song_id) FROM Song))")
                
conn.commit()
conn.close()