import os
import sqlite3
from flask import Flask, request, abort, jsonify, send_from_directory

sqlite_file = 'Jukebox.db'
# Connecting to the database file
UPLOAD_DIRECTORY = r'C:\Users\mrk0021\Documents\Jukebox\Python Rest Api'

if not os.path.exists(UPLOAD_DIRECTORY):
    os.makedirs(UPLOAD_DIRECTORY)


api = Flask(__name__)


@api.route('/files')
def list_files():
    """Endpoint to list files on the server."""
    a = {} 
    f = []
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    sqlAlbum = c.execute("select album_name from Album").fetchall()    
    sql = c.execute("select s.Song_name, a.Album_name, s.Song_path, s.Song_image, a.Album_id, s.Song_duration from Song s join B_Album_Song bas on s.Song_id = bas.Song_Id join Album a on bas.Album_id = a.Album_ID").fetchall()
    for albumname in sqlAlbum:
        a = [{"albumId": y[4], "songName": y[0], "songPath": y[2]+"\\"+y[0], "songImage": y[3], "songDuration": int(float(y[5]))} for y in sql if y[1] == albumname[0] ]   
        f.append({"albumName":albumname[0],"songs":a}) 
        
    print(a[0])
    return jsonify({'data': f})


@api.route('/files/<path:path>')
def get_file(path):
    """Download a file."""
    path.replace('%20', ' ')
    path, song = os.path.split(path)
    return send_from_directory(path, song)


@api.route('/files/<filename>', methods=['POST'])
def post_file(filename):
    """Upload a file."""

    if '/' in filename:
        # Return 400 BAD REQUEST
        abort(400, 'no subdirectories directories allowed')

    with open(os.path.join(UPLOAD_DIRECTORY, filename), 'wb') as fp:
        fp.write(request.data)

    # Return 201 CREATED
    return '', 201


if __name__ == '__main__':
    api.run(debug=True, port=8000)